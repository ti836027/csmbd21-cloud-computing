
##################################### FUNCTION #####################################
def mapper(x,y): 
    #list where out of mapper will be stored
    mapper = []

    # loop to iternate through file
    for record in open(file_path):
        #stripping if there is any white space
        record = record.strip()
        #splitting data from the file by comma
        record = record.split(',') 
        #putting required key value to use in a variable
        result=('%s,%s'%(record[x],y))
        #appending the value of varible in a list
        mapper.append(result)
        #returning the output of mapper
    return mapper

def shuffle(a):
    #new list to store sorted data
    words=list()

    #iterating through mapper output
    for line in a:
        #stripping and splitting the data
        word=line.strip().split()
        #appending the data in list
        words.append(word)

    #sorting the data alphabatically
    words.sort()
    return words

def reducer(b):
    #new dictionary to store reduced data
    data_dict={}
    
    #summing number of flights in each passenger id
    for listt in b:
        for info in listt:
            passenger,flight = info.split(',')
            
            if passenger in data_dict:
                flight = data_dict.get(passenger)
                flight = int(flight)
                flight += 1
                data_dict[passenger] = flight
            else:
                data_dict[passenger] = flight
    return data_dict

####################################################################################

#importing libraries 
import sys

#location file
file_path = "AComp_Passenger_data_no_error_DateTime.csv"

#calling function mapper -> sorter -> reducer 
map_out = mapper(0,1)

reduce_in = shuffle(map_out)

reduce_out = reducer(reduce_in)

#printing the output
print('the passenger id with',max(reduce_out, key=reduce_out.get),'has the highest number of flights which is total', reduce_out.get(max(reduce_out, key=reduce_out.get)))

####################################################################################

